{ nixpkgs ? import <nixpkgs> {  }}:

let

  version = "0.0.1";

in nixpkgs.stdenv.mkDerivation rec {

  pname = "rs-aoc-2021";
  inherit version;

  nativeBuildInputs = [
    nixpkgs.rustup nixpkgs.cargo
  ];
  
  shellHook = ''
    export PATH=$HOME/.cargo/bin:$PATH
  '';

  meta = with nixpkgs.stdenv.lib; {
    description = "AoC 2021 entry";
    homepage = "https://gitlab.com/kblobr/rs-aoc-2021";
  };
}

