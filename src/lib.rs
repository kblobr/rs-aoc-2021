#![deny(warnings)]

use std::collections::HashMap;

pub fn day_1_part_1(arr: &[i32]) -> i32 {
    arr.windows(2).fold(0, |acc, pair| {
        acc + match pair[0] < pair[1] {
            true => 1,
            _ => 0,
        }
    })
}

pub fn day_1_part_2(arr: &[i32]) -> i32 {
    arr.windows(4).fold(0, |acc, quads| {
        acc + match (quads[0] + quads[1] + quads[2]) < (quads[1] + quads[2] + quads[3]) {
            true => 1,
            _ => 0,
        }
    })
}

pub fn day_2_part_1(moves: &[(&str, i32)]) -> i32 {
    let (depth, position) = moves.iter().fold((0, 0), |acc, item| match item.0 {
        "forward" => (acc.0, acc.1 + item.1),
        "down" => (acc.0 + item.1, acc.1),
        "up" => (acc.0 - item.1, acc.1),
        m => panic!("Unkown movement {}", m),
    });
    depth * position
}

pub fn day_2_part_2(moves: &[(&str, i32)]) -> i32 {
    let (depth, position, _aim) = moves.iter().fold((0, 0, 0), |acc, item| match item.0 {
        "forward" => (acc.0 + (acc.2 * item.1), acc.1 + item.1, acc.2),
        "down" => (acc.0, acc.1, acc.2 + item.1),
        "up" => (acc.0, acc.1, acc.2 - item.1),
        m => panic!("Unkown movement {}", m),
    });
    depth * position
}

pub fn day_3_part_1(data: &[&str]) -> i32 {
    let half_way = data.len() as i32 / 2;
    let digit_count = data.iter().fold(Vec::<i32>::new(), |mut acc, line| {
        for (i, c) in line.char_indices() {
            if acc.len() == i {
                acc.push(0);
            }
            acc[i] += c as i32 - '0' as i32;
        }
        acc
    });
    let mut gamma = 0;
    let mut epsilon = 0;

    for i in 0..digit_count.len() {
        if digit_count[i] / half_way == 1 {
            gamma += 1 << (digit_count.len() - 1 - i);
        } else {
            epsilon += 1 << (digit_count.len() - 1 - i);
        }
    }
    gamma * epsilon
}

pub fn day_3_part_2(data: &[&str]) -> i32 {
    let mut o2_ratings = data.to_vec();
    let mut co2_ratings = data.to_vec();
    let number_size = data.first().unwrap().chars().count();
    for i in 0..number_size {
        let size = o2_ratings.len() as i32;
        let count_of_ones = o2_ratings.iter().fold(0, |sum, line| {
            sum + line.chars().nth(i).unwrap() as i32 - '0' as i32
        });
        let char_to_keep = if count_of_ones >= size - count_of_ones {
            '1'
        } else {
            '0'
        };
        o2_ratings = o2_ratings
            .iter()
            .filter(|line| line.chars().nth(i).unwrap() == char_to_keep)
            .cloned()
            .collect();
        if o2_ratings.len() == 1 {
            break;
        }
    }

    for i in 0..number_size {
        let size = co2_ratings.len() as i32;
        let count_of_ones = co2_ratings.iter().fold(0, |sum, line| {
            sum + line.chars().nth(i).unwrap() as i32 - '0' as i32
        });
        let char_to_keep = if count_of_ones >= size - count_of_ones {
            '0'
        } else {
            '1'
        };
        co2_ratings = co2_ratings
            .iter()
            .filter(|line| line.chars().nth(i).unwrap() == char_to_keep)
            .cloned()
            .collect();
        if co2_ratings.len() == 1 {
            break;
        }
    }
    i32::from_str_radix(o2_ratings.first().unwrap(), 2).unwrap()
        * i32::from_str_radix(co2_ratings.first().unwrap(), 2).unwrap()
}

pub fn day_4_part_1(draws: &[i32], boards: &[&[&[i32]]]) -> i32 {
    for i in 4..draws.len() {
        let drawn = &draws[0..i];
        for board in boards {
            let has_winning_row = board
                .iter()
                .any(|row| row.iter().all(|number| drawn.contains(number)));
            let has_winning_column = (0..4).any(|col| {
                drawn.contains(&board[0][col])
                    && drawn.contains(&board[1][col])
                    && drawn.contains(&board[2][col])
                    && drawn.contains(&board[3][col])
                    && drawn.contains(&board[4][col])
            });
            if has_winning_column || has_winning_row {
                let last_drawn: i32 = *drawn.last().unwrap();
                let sum_of_unmarked: i32 = board
                    .iter()
                    .map(|row| row.iter().filter(|n| !drawn.contains(n)).sum::<i32>())
                    .sum::<i32>();
                return last_drawn * sum_of_unmarked;
            }
        }
    }
    0
}

pub fn day_4_part_2(draws: &[i32], boards: &[&[&[i32]]]) -> i32 {
    let mut winning_boards = Vec::<usize>::new();
    for i in 4..draws.len() {
        let drawn = &draws[0..i];
        for board_index in 0..boards.len() {
            if winning_boards.contains(&board_index) {
                continue;
            }
            let board = boards[board_index];
            let has_winning_row = board
                .iter()
                .any(|row| row.iter().all(|number| drawn.contains(number)));
            let has_winning_column = (0..4).any(|col| {
                drawn.contains(&board[0][col])
                    && drawn.contains(&board[1][col])
                    && drawn.contains(&board[2][col])
                    && drawn.contains(&board[3][col])
                    && drawn.contains(&board[4][col])
            });
            if has_winning_column || has_winning_row {
                if boards.len() - winning_boards.len() == 1 {
                    let last_drawn: i32 = *drawn.last().unwrap();
                    let sum_of_unmarked: i32 = board
                        .iter()
                        .map(|row| row.iter().filter(|n| !drawn.contains(n)).sum::<i32>())
                        .sum::<i32>();
                    return last_drawn * sum_of_unmarked;
                } else {
                    winning_boards.push(board_index);
                }
            }
        }
    }
    0
}

type _Point = (i32, i32);

pub fn day_5_part_1(lines: &[(_Point, _Point)]) -> usize {
    let mut diagram = HashMap::<_Point, i32>::new();
    lines
        .iter()
        .filter(|line| line.0 .0 == line.1 .0 || line.0 .1 == line.1 .1)
        .for_each(|line| {
            let origin: _Point = match line.0 .0 < line.1 .0 || line.0 .1 < line.1 .1 {
                true => line.0,
                _ => line.1,
            };
            let destination = match line.0 .0 > line.1 .0 || line.0 .1 > line.1 .1 {
                true => line.0,
                _ => line.1,
            };
            for x in origin.0..=destination.0 {
                for y in origin.1..=destination.1 {
                    let counter = diagram.entry((x, y)).or_insert(0);
                    *counter += 1;
                }
            }
        });
    diagram.into_values().filter(|v| v > &1).count()
}

pub fn day_5_part_2(lines: &[(_Point, _Point)]) -> usize {
    let mut diagram = HashMap::<_Point, i32>::new();
    for line in lines {
        let mut expanded_line: Vec<_Point> = vec![];
        if line.0 .0 == line.1 .0 {
            //horizontal
            let range = if line.0 .1 > line.1 .1 {
                line.1 .1..=line.0 .1
            } else {
                line.0 .1..=line.1 .1
            };
            for y in range {
                expanded_line.push((line.0 .0, y));
            }
        } else if line.0 .1 == line.1 .1 {
            //vertical
            let range = if line.0 .0 > line.1 .0 {
                line.1 .0..=line.0 .0
            } else {
                line.0 .0..=line.1 .0
            };
            for x in range {
                expanded_line.push((x, line.0 .1));
            }
        } else if (line.0 .0 - line.1 .0).abs() == (line.0 .1 - line.1 .1).abs() {
            //diagonal
            for step in 0..=(line.0 .0 - line.1 .0).abs() {
                let x = if line.0 .0 < line.1 .0 {
                    line.0 .0 + step
                } else {
                    line.0 .0 - step
                };
                let y = if line.0 .1 < line.1 .1 {
                    line.0 .1 + step
                } else {
                    line.0 .1 - step
                };
                expanded_line.push((x, y));
            }
        } else {
            continue;
        };
        for (x, y) in expanded_line {
            let counter = diagram.entry((x, y)).or_insert(0);
            *counter += 1;
        }
    }
    diagram.into_values().filter(|&v| v > 1).count()
}

pub fn day_6_part_1(population: &mut Vec<i32>, days: i32) -> usize {
    for _ in 0..days {
        let new_pop_size = population.len() + population.iter().filter(|&&f| f == 0).count();
        population
            .iter_mut()
            .filter(|f| **f == 0)
            .for_each(|f| *f = 7);
        population.iter_mut().for_each(|f| *f -= 1);
        population.resize(new_pop_size, 8);
    }
    population.len()
}

pub fn day_6_part_2(population: &[usize], days: i32) -> usize {
    let mut pop_map = [0; 9];
    for &fish in population {
        pop_map[fish] += 1;
    }
    for _ in 0..days {
        pop_map.rotate_left(1);
        pop_map[6] += pop_map[8];
    }
    pop_map.iter().sum()
}

pub fn day_7_part_1(positions: &mut [i32]) -> i32 {
    positions.sort_unstable();
    let middle_point = positions.len() / 2;
    let mean = match positions.len() % 2 {
        0 => (positions[middle_point - 1] + positions[middle_point]) / 2,
        _ => positions[middle_point],
    };
    positions.iter().fold(0, |acc, p| acc + (*p - mean).abs())
}

pub fn day_7_part_2(positions: &[i32]) -> i32 {
    let median: i32 = (f64::from(positions.iter().sum::<i32>()) / f64::from(positions.len() as i32))
        .ceil() as i32;
    let mut lowest = -1;
    for m in median - 1..=median + 1 {
        let answer = positions.iter().fold(0, |acc, &p| {
            let diff = (p - m).abs();
            acc + ((diff * (diff + 1)) / 2)
        });
        if lowest == -1 || lowest > answer {
            lowest = answer;
        }
    }
    lowest
}

fn _count_unique_segments(uniques: &[usize], sequences: &[&str]) -> usize {
    sequences
        .iter()
        .filter(|&&s| uniques.contains(&s.len()))
        .count()
}

fn _get_risk_level(heightmap: &[&[i32]]) -> i32 {
    let mut risk_level = 0;
    for y in 0..heightmap.len() {
        let row = heightmap[y];
        for x in 0..row.len() {
            let up = if y > 0 { heightmap[y - 1][x] } else { 9 };
            let down = if y < heightmap.len() - 1 {
                heightmap[y + 1][x]
            } else {
                9
            };
            let left = if x > 0 { heightmap[y][x - 1] } else { 9 };
            let right = if x < row.len() - 1 {
                heightmap[y][x + 1]
            } else {
                9
            };
            let current = heightmap[y][x];
            if current >= up || current >= down || current >= left || current >= right {
                continue;
            }
            risk_level += current + 1;
        }
    }
    risk_level
}

fn _score_syntax(lines: &[&str]) -> i32 {
    lines.iter().fold(0, |acc, &line| {
        let mut open_tokens = Vec::<char>::new();
        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => open_tokens.push(c),

                ')' => match open_tokens.pop() {
                    Some('(') => continue,
                    _ => return acc + 3,
                },
                ']' => match open_tokens.pop() {
                    Some('[') => continue,
                    _ => return acc + 57,
                },
                '}' => match open_tokens.pop() {
                    Some('{') => continue,
                    _ => return acc + 1197,
                },
                '>' => match open_tokens.pop() {
                    Some('<') => continue,
                    _ => return acc + 25137,
                },
                _ => panic!("illegal character"),
            }
        }
        acc
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn d1_p1() {
        // https://adventofcode.com/2021/day/1
        // Sonar Sweep
        assert_eq!(
            day_1_part_1(&[199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
            7
        );
    }

    #[test]
    fn d1_p2() {
        // https://adventofcode.com/2021/day/1#part2
        // Sonar Sweep window
        assert_eq!(
            day_1_part_2(&[199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
            5
        );
    }

    #[test]
    fn d2_p1() {
        // https://adventofcode.com/2021/day/2
        // Dive
        assert_eq!(
            day_2_part_1(&[
                ("forward", 5),
                ("down", 5),
                ("forward", 8),
                ("up", 3),
                ("down", 8),
                ("forward", 2),
            ]),
            150
        );
    }

    #[test]
    fn d2_p2() {
        // https://adventofcode.com/2021/day/2#part2
        // Dive
        assert_eq!(
            day_2_part_2(&[
                ("forward", 5),
                ("down", 5),
                ("forward", 8),
                ("up", 3),
                ("down", 8),
                ("forward", 2),
            ]),
            900
        );
    }

    #[test]
    fn d3_p1() {
        // https://adventofcode.com/2021/day/3
        // Binary Diagnostic
        assert_eq!(
            day_3_part_1(&[
                "00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000",
                "11001", "00010", "01010",
            ]),
            198
        );
    }

    #[test]
    fn d3_p2() {
        // https://adventofcode.com/2021/day/3#part2
        // Binary Diagnostic
        assert_eq!(
            day_3_part_2(&[
                "00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000",
                "11001", "00010", "01010",
            ]),
            230
        );
    }

    #[test]
    fn d4_p1() {
        // https://adventofcode.com/2021/day/4
        // Giant squid
        assert_eq!(
            day_4_part_1(
                &[
                    7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18,
                    20, 8, 19, 3, 26, 1,
                ],
                &[
                    &[
                        &[22, 13, 17, 11, 0],
                        &[8, 2, 23, 4, 24],
                        &[21, 9, 14, 16, 7],
                        &[6, 10, 3, 18, 5],
                        &[1, 12, 20, 15, 19],
                    ],
                    &[
                        &[3, 15, 0, 2, 22],
                        &[9, 18, 13, 17, 5],
                        &[19, 8, 7, 25, 23],
                        &[20, 11, 10, 24, 4],
                        &[14, 21, 16, 12, 6],
                    ],
                    &[
                        &[14, 21, 17, 24, 4],
                        &[10, 16, 15, 9, 19],
                        &[18, 8, 23, 26, 20],
                        &[22, 11, 13, 6, 5],
                        &[2, 0, 12, 3, 7],
                    ]
                ]
            ),
            4512
        );
    }

    #[test]
    fn d4_p2() {
        // https://adventofcode.com/2021/day/4#part2
        // Giant squid
        assert_eq!(
            day_4_part_2(
                &[
                    7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18,
                    20, 8, 19, 3, 26, 1,
                ],
                &[
                    &[
                        &[22, 13, 17, 11, 0],
                        &[8, 2, 23, 4, 24],
                        &[21, 9, 14, 16, 7],
                        &[6, 10, 3, 18, 5],
                        &[1, 12, 20, 15, 19],
                    ],
                    &[
                        &[3, 15, 0, 2, 22],
                        &[9, 18, 13, 17, 5],
                        &[19, 8, 7, 25, 23],
                        &[20, 11, 10, 24, 4],
                        &[14, 21, 16, 12, 6],
                    ],
                    &[
                        &[14, 21, 17, 24, 4],
                        &[10, 16, 15, 9, 19],
                        &[18, 8, 23, 26, 20],
                        &[22, 11, 13, 6, 5],
                        &[2, 0, 12, 3, 7],
                    ]
                ]
            ),
            1924
        );
    }

    #[test]
    fn d5_p1() {
        // https://adventofcode.com/2021/day/5
        // Hydrothermal Venture
        assert_eq!(
            day_5_part_1(&[
                ((0, 9), (5, 9)),
                ((8, 0), (0, 8)),
                ((9, 4), (3, 4)),
                ((2, 2), (2, 1)),
                ((7, 0), (7, 4)),
                ((6, 4), (2, 0)),
                ((0, 9), (2, 9)),
                ((3, 4), (1, 4)),
                ((0, 0), (8, 8)),
                ((5, 5), (8, 2)),
            ]),
            5
        );
    }

    #[test]
    fn d5_p2() {
        // https://adventofcode.com/2021/day/5#part2
        // Hydrothermal Venture
        assert_eq!(
            day_5_part_2(&[
                ((0, 9), (5, 9)),
                ((8, 0), (0, 8)),
                ((9, 4), (3, 4)),
                ((2, 2), (2, 1)),
                ((7, 0), (7, 4)),
                ((6, 4), (2, 0)),
                ((0, 9), (2, 9)),
                ((3, 4), (1, 4)),
                ((0, 0), (8, 8)),
                ((5, 5), (8, 2)),
            ]),
            12
        );
    }

    #[test]
    fn d6_p1() {
        // https://adventofcode.com/2021/day/6
        // Lanternfish
        assert_eq!(day_6_part_1(&mut vec![3, 4, 3, 1, 2], 18), 26);
        assert_eq!(day_6_part_1(&mut vec![3, 4, 3, 1, 2], 80), 5934);
    }

    #[test]
    fn d6_p2() {
        // https://adventofcode.com/2021/day/6#part2
        // Lanternfish
        assert_eq!(day_6_part_2(&[3, 4, 3, 1, 2], 18), 26);
        assert_eq!(day_6_part_2(&[3, 4, 3, 1, 2], 80), 5934);
        assert_eq!(day_6_part_2(&[3, 4, 3, 1, 2], 256), 26984457539);
    }

    #[test]
    fn d7_p1() {
        // https://adventofcode.com/2021/day/7
        // The Treachery of Whales
        assert_eq!(day_7_part_1(&mut [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 37);
    }

    #[test]
    fn d7_p2() {
        // https://adventofcode.com/2021/day/7#part2
        // The Treachery of Whales
        assert_eq!(day_7_part_2(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 168);
    }

    #[test]
    fn day_8() {
        // https://adventofcode.com/2021/day/8
        // Seven Segment Search
        assert_eq!(
            _count_unique_segments(
                &[2, 3, 4, 7],
                &[
                    "fdgacbe", "cefdb", "cefbgd", "gcbe", "fcgedb", "cgb", "dgebacf", "gc", "cg",
                    "cg", "fdcagb", "cbg", "efabcd", "cedba", "gadfec", "cb", "gecf", "egdcabf",
                    "bgf", "bfgea", "gebdcfa", "ecba", "ca", "fadegcb", "cefg", "dcbef", "fcge",
                    "gbcadfe", "ed", "bcgafe", "cdgba", "cbgef", "gbdfcae", "bgc", "cg", "cgb",
                    "fgae", "cfgab", "fg", "bagce",
                ]
            ),
            26
        );
    }

    #[test]
    fn day_9() {
        // https://adventofcode.com/2021/day/9
        // Smoke Basin
        assert_eq!(
            _get_risk_level(&[
                &[2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
                &[3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
                &[9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
                &[8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
                &[9, 8, 9, 9, 9, 6, 5, 6, 7, 8],
            ]),
            15
        );
    }

    #[test]
    fn day_10() {
        // https://adventofcode.com/2021/day/10
        // Syntax Scoring
        assert_eq!(
            _score_syntax(&[
                "[({(<(())[]>[[{[]{<()<>>",
                "[(()[<>])]({[<{<<[]>>(",
                "{([(<{}[<>[]}>{[]{[(<()>",
                "(((({<>}<{<{<>}{[]{[]{}",
                "[[<[([]))<([[{}[[()]]]",
                "[{[{({}]{}}([{[{{{}}([]",
                "{<[[]]>}<{[{[{[]{()[[[]",
                "[<(<(<(<{}))><([]([]()",
                "<{([([[(<>()){}]>(<<{{",
                "<{([{{}}[<[[[<>{}]]]>[]]",
            ]),
            26397
        );
    }
}
